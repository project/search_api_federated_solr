<?php

namespace Drupal\search_api_federated_solr\EventSubscriber;

use Drupal\search_api_solr\Event\SearchApiSolrEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PostCreateIndexDocumentsEventSubscriber.
 *
 * @package Drupal\search_api_federated_solr\EventSubscriber
 */
class PostCreateIndexDocumentsEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      SearchApiSolrEvents::POST_CREATE_INDEX_DOCUMENTS => 'postCreateIndexedDocuments',
    ];
  }

  /**
   * React to a config object being saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function postCreateIndexedDocuments($event) {
    $documents = $event->getSolariumDocuments();

    foreach ($documents as $document) {
      $fields = $document->getFields();

      foreach ($fields as $field_id => $field) {
        $default_language_id = \Drupal::languageManager()->getDefaultLanguage()->getId();
        $string = $default_language_id . '_rendered_item';

        if (substr_count($field_id, $string) > 0 && substr_count($field_id, 'sort_') === 0) {
          $boost = $document->getFieldBoost($field_id);
          $modifier = $document->getFieldModifier($field_id);

          // The 'content' field is the generic handler for the Drupal 7 schema version.
          if (is_array($field)) {
            $string = implode(" ", $field);
            if (!is_null($string)) {
              $document->setField('content', $string, $boost, $modifier);
            }
          }
          $document->setField('tm_rendered_item', $field, $boost, $modifier);
        }
      }
    }

    $event->setSolariumDocuments($documents);
  }

}